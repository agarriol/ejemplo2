class CommentMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.comment_mailer.send_mail.subject
  #
  def send_mail(user)
    @user = user
    mail to: @user.email, subject: 'Nuevo comentario'
  end

  def send_mail_bad_comment(comment)
    @greeting = comment #En greeting se añade lo que se quiere añadir al comentario
    mail to: 'admin@email.com', subject: 'Comentario ofensivo'
  end
end

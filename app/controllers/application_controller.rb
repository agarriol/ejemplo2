class ApplicationController < ActionController::API
  # include JSONAPI::ActsAsResourceController # Gema json antigua
  include RailsJwtAuth::WardenHelper
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :render_403

  def render_403
    render json: {}, status: 403
  end
end

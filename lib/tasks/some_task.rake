# require './app/controllers/application_controller.rb'
# require './app/controllers/comments_controller.rb'

namespace :some_task do
  desc 'List versions of all Rails frameworks and the environment'
  task about: :environment do
    @comments = Comment.all

    @bad_comments = ''

    @comments.each do |c| #que muestre solo el que tenga la palabra nuevo
      word = c.content_comment.split(' ')
      word.each do |w|
        if w == 'NUEVO' || w == 'nuevo'
          puts c.content_comment
          @bad_comments += c.content_comment + "\n\r"
        end
      end
    end
    CommentMailer.send_mail_bad_comment(@bad_comments).deliver_now
  end
end

class Micropost < ApplicationRecord
  belongs_to :user
  has_many :comments
  mount_uploader :picture, PictureUploader
  # validates :user_id, presence: true
  validates :content, presence: true
end

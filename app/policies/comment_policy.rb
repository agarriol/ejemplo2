class CommentPolicy < ApplicationPolicy
    attr_reader :user, :comment
  
    def initialize(user, comment)
      @user = user
      @comment = comment
    end
  
    def destroy?
      # Si es true lo permite, sino no
      user.id == comment.micropost.user_id || (user.has_role? :admin)
    end
  
    class Scope < Scope
      def resolve
        scope
      end
    end
  end
  
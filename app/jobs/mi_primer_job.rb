class MiPrimerJob < ApplicationJob
  queue_as :default

  def perform(comment)
    # recibir el comentario y buscar palabras ofensivas (es decir"nuevo")
    puts '-' * 100 + 'Si que se ha llamado al job'

    @comment = comment.content_comment
    word = @comment.split(' ')
    word.each do |w|
      if w == 'NUEVO' || w == 'nuevo'
        puts @comment
        CommentMailer.send_mail_bad_comment(@comment).deliver_now
      end
    end

  end
end

class MicropostPolicy < ApplicationPolicy
  attr_reader :user, :micropost

  def initialize(user, micropost)
    @user = user
    @micropost = micropost
  end

  def destroy?
    # true
    if user.id == micropost.user_id
      return true
    elsif user.has_role? :admin
      return true
    else
      return  false
    end
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end

class MicropostsController < ApplicationController
  before_action :set_micropost, only: [:show, :update, :destroy]
  before_action :authenticate! # El usuario necesita autenticarse
  # deserializable_resource :post, only: [:create, :update]

  # GET /microposts
  def index

    @microposts = Micropost.all
    if params[:filter]
      if params[:filter][:user]
        @microposts = @microposts.where(user_id: params[:filter][:user])
      end
      if params[:filter][:content]
        #con include? o algo así 
        @microposts = @microposts.where(content: params[:filter][:content])
      end
      if params[:filter][:FechaFin]
        @microposts = @microposts.where(
          "created_at <= :end_date",{end_date: params[:filter][:FechaFin]}
        )
      end
      if params[:filter][:FechaInicio]
        @microposts = @microposts.where(
          "created_at >= :start_date",{start_date: params[:filter][:FechaInicio]}
        )
      end
    end
    render jsonapi: @microposts.order(created_at: :asc)
  end

  # GET /microposts/1
  def show
    render jsonapi: @micropost
  end

  # POST /microposts
  def create
    @micropost = Micropost.new(micropost_params)
    @micropost.user = current_user

    if @micropost.save
      render jsonapi: @micropost, status: :created # Respuesta HHTP
    else
      render jsonapi_errors: @micropost.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /microposts/1
  def update
    if @micropost.update(micropost_params)
      render jsonapi: @micropost
    else
      render jsonapi: @micropost.errors, status: :unprocessable_entity
    end
  end

  # DELETE /microposts/1
  def destroy
    authorize @micropost
    @micropost.destroy
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_micropost
      @micropost = Micropost.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def micropost_params
      params.require(:micropost).permit(:content, :picture)
    end
end

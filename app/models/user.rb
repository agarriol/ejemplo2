class User < ApplicationRecord
  rolify
  include RailsJwtAuth::Authenticatable
  has_many :microposts

  # has_secure_password
  # validates :password, presence: true
  # A los usuarios por defecto se les añade el rol newuser
  after_create :assign_default_role

  def assign_default_role
    add_role(:newuser) if roles.blank?
  end
end

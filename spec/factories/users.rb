FactoryBot.define do
  factory :user do
    name 'John'
    email 'john1.doe@example.com'
    password 'pruebasdfg'
  end # Debería tener user_id '1'...pero se pondrá solo digo yo

  factory :micropost do
    content 'Im number 1, post 1'
    user_id '980190962'
  end
end

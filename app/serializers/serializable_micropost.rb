class SerializableMicropost < JSONAPI::Serializable::Resource
  type 'micropost'

  attributes :content, :user_id, :created_at, :updated_at, :picture
  # Para cambiar el nombre del valor:
  # attribute :created_at do
  #   @object.created_at
  # end
  # attribute :updated_at do
  #   @object.updated_at
  # end
end

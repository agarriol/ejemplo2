class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.string :content_comment
      t.integer :micropost_id

      t.timestamps
    end
  end
end
